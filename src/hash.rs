use crate::wordlist::Wordlist;
use sha2::{Digest, Sha256};

#[derive(Debug)]
pub struct Hash {
    pub original_hash: String,
    pub result: String,
}

impl Hash {
    pub fn new(hash: String, wordlist: Wordlist) -> Result<Self, anyhow::Error> {
        for line in &wordlist.content {
            let line_hash = format!("{:x}", Sha256::digest(line.as_bytes()));

            if line_hash == hash {
                return Ok(Hash {
                    original_hash: hash,
                    result: line.to_string(),
                });
            }
        }

        Err(anyhow::Error::msg("Hash not found in wordlist"))

        // Ok(Hash {
        //     wordlist,
        //     original_hash: hash,
        //     result: String::new(),
        // })
    }
}
